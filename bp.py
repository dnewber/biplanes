import sys
import math
import pygame
from pygame.locals import *

dev_notes = '''
- Momentum Updated - cleaner and more realistic with sin()
- detach rotation from angle of travel
- need to eliminate backwards flight
'''

FPS = 30
WINDOWWIDTH = 740
WINDOWHEIGHT = 480

#            R    G    B
GRAY     = (100, 100, 100)
NAVYBLUE = ( 60,  60, 100)
WHITE    = (255, 255, 255)
RED      = (255,   0,   0)
GREEN    = (  0, 176,   0)
BLUE     = (  0,   0, 255)
YELLOW   = (255, 255,   0)
ORANGE   = (255, 128,   0)
PURPLE   = (255,   0, 255)
SKYBLUE  = (152, 156, 252)

BGCOLOR = SKYBLUE

PLANE_MASS = 0.095  # Must be less than 1
ENGINE_STRENGTH = 2  # (Movement Increment) Higher is faster and wider turns


def main():
    global FPSCLOCK, DISPLAYSURF
    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    pygame.key.set_repeat(50, 50)  # enable held down keys

    REDSURF = pygame.Surface((40, 17))
    GREENSURF = pygame.Surface((40, 17))

    REDspeed = 0  # need to push button to "take-off"
    GREENspeed = 0

    red_start_position = (50, 430)
    redx, redy = red_start_position
    green_start_position = (650, 430)
    greenx, greeny = green_start_position

    red_draw_x, red_draw_y = (0, 7)  # position on plane surface
    green_draw_x, green_draw_y = (0, 7)
    rotateDegree = 0
    green_rotateDegree = 0

    redReady = True  # to prevent multiple "take-off launches" while flying
    greenReady = True

    red_score = 0
    green_score = 0
    red_bullet_count = 0

    while True:  # main loop
        DISPLAYSURF.fill(BGCOLOR)
        REDSURF.fill(BGCOLOR)
        GREENSURF.fill(BGCOLOR)
        REDSURF.set_colorkey(BGCOLOR)  # Makes transparent background for planes
        GREENSURF.set_colorkey(BGCOLOR)

        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
                pygame.quit()
                sys.exit()
            elif event.type == KEYDOWN:
                # RED CONTROLS
                if event.key == K_x and redReady == False:
                    rotateDegree += 22.5
                if event.key == K_s and redReady == False:
                    rotateDegree -= 22.5
                if event.key == K_SPACE and REDspeed == 0 and redReady == True:  # take-off launch
                    REDspeed = 4
                    redReady = False
                if event.key == K_b and redReady == False:  # shoot
                    red_bullet_count += 1
                    red_shootx = redx  # captures red location
                    red_shooty = redy
                    red_shoot_angle = rotateDegree

                # GREEN CONTROLS
                if event.key == K_UP and greenReady == False:
                    green_rotateDegree += 22.5
                if event.key == K_DOWN and greenReady == False:
                    green_rotateDegree -= 22.5
                if event.key == K_KP0 and GREENspeed == 0 and greenReady == True:  # take-off launch
                    GREENspeed = -4
                    greenReady = False

        # Infinite screen and Top limit
        if redx > 740:
            redx = 1
        if redx < 0:
            redx = 739
        if redy < 0:
            REDspeed = 0
        if greenx > 740:
            greenx = 1
        if greenx < 0:
            greenx = 739
        if greeny < 0:
            GREENspeed = 0

        # Crash into ground
        if redy > 463:
            rotateDegree = 0
            REDspeed = 0
            redReady = True
            redx, redy = red_start_position
            green_score += 1
        if greeny > 463:
            green_rotateDegree = 0
            GREENspeed = 0
            greenReady = True
            greenx, greeny = green_start_position
            red_score += 1

        # Fixes spin
        if rotateDegree >= 360 or rotateDegree <= -360:
            rotateDegree = 0
        if green_rotateDegree >= 360 or green_rotateDegree <= -360:
            green_rotateDegree = 0

        #Prevents reverse flying - BUGGY
        #if REDspeed < -2:
            #REDspeed = 0
            #rotateDegree += 22.5
        #if GREENspeed > 2:
            #GREENspeed = 0
            #green_rotateDegree -= 22.5

        # Shoot!
        if red_bullet_count > 0:
            red_bullet_angle = bullet(WHITE, red_shoot_angle)
            red_shootx, red_shooty = flightDirection(red_shootx, red_shooty, red_bullet_angle, 4, 2)  # 4 is speed, 1 is move increment
            DISPLAYSURF.blit(red_bullet_angle, (red_shootx, red_shooty))

        drawPlane(REDSURF, red_draw_x, red_draw_y, RED)
        rotatedRed = pygame.transform.rotate(REDSURF, rotateDegree)
        DISPLAYSURF.blit(rotatedRed, (redx, redy))

        REDspeed = momentum(rotateDegree, REDspeed, PLANE_MASS)
        redx, redy = flightDirection(redx, redy, rotateDegree, REDspeed, ENGINE_STRENGTH)

        drawPlane(GREENSURF, green_draw_x, green_draw_y, GREEN)
        GREENSURF_flipped = pygame.transform.flip(GREENSURF, True, False)
        rotatedGreen = pygame.transform.rotate(GREENSURF_flipped, green_rotateDegree)
        DISPLAYSURF.blit(rotatedGreen, (greenx, greeny))

        GREENspeed = momentum(green_rotateDegree, GREENspeed, PLANE_MASS)
        greenx, greeny = flightDirection(greenx, greeny, green_rotateDegree, GREENspeed, ENGINE_STRENGTH)

        #----TEST STUFF---- DELETE WHEN COMPLETE--------
        fontObj = pygame.font.Font('freesansbold.ttf', 32)
        textSurfaceObj = fontObj.render(str(red_score), True, WHITE, BLUE)
        textRectObj = textSurfaceObj.get_rect()
        DISPLAYSURF.blit(textSurfaceObj, textRectObj)
        #----TEST STUFF---------------------------------

        pygame.display.update()
        FPSCLOCK.tick(FPS)


def drawPlane(surface, posx, posy, color):
    pygame.draw.rect(surface, color, (posx, posy, 40, 5), 0)  # main
    pygame.draw.rect(surface, color, (posx, (posy - 3), 5, 6), 0)  # tail section
    pygame.draw.rect(surface, color, (posx + 15, posy - 7, 15, 5), 0)  # wing
    pygame.draw.rect(surface, color, (posx + 24, posy + 7, 6, 3), 0)  # landing gear


def momentum(angle, initial_speed, PLANE_MASS):
    current_speed = 0
    radians = math.radians(angle)
    velocity = abs(math.sin(radians))
    momentum = velocity * PLANE_MASS

    # slow
    if (angle > 0 and angle < 180) or (angle > -360 and angle < -180):
        current_speed = initial_speed - momentum
    # fast
    elif (angle < 360 and angle > 180) or (angle < 0 and angle > -180):
        current_speed = initial_speed + momentum
    # level
    else:
        current_speed = initial_speed

    ###SPEED LIMITER### Comment out for faster flight
    if current_speed > 5:
        current_speed = 5
    if current_speed < -5:
        current_speed = -5

    return current_speed


def flightDirection(posx, posy, angle, speed, move_incr):

    if (angle >= 0 and angle <= 90) or (angle <= -270 and angle >= -360): # Up-Right
        posx += move_incr * speed
        posy -= move_incr * speed
    if (angle <= 0 and angle >= -90) or (angle >= 270 and angle <= 360): # Down-Right
        posx += move_incr * speed
        posy += move_incr * speed
    if (angle >= 90 and angle <= 180) or (angle >= -270 and angle <= -180): # Up-Left
        posx -= move_incr * speed
        posy -= move_incr * speed
    if (angle >= 180 and angle <= 270) or (angle >= -180 and angle <= -90): # Down-Left
        posx -= move_incr * speed
        posy += move_incr * speed
    if speed >= -1 and speed <= 1 and speed != 0:  # Causes a stall at low speed
        posy += 2

    return (posx, posy)


def crashAnimation(color, x, y,):
    pass


def bullet(color, rotateDegree):  # Only makes surface, draws bullet, then rotates.
    BULLETSURF = pygame.Surface((5, 5))  # make bullet surface #maybe just draw to background instead?
    BULLETSURF.fill(color)  # fill b surface
    rotated_bullet = pygame.transform.rotate(BULLETSURF, rotateDegree)  # rotate bullet to match plane
    return rotated_bullet


if __name__ == '__main__':
    main()