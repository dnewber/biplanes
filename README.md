# PyPlanes #
## A Python port of "Biplanes" from the 1979 Intellivision video game console ##

I've checked this into version control just for nostalgia's sake, as it was the second non-guided project I attempted with Python (circa 2012). It was written on a Windows 7 pc with Python 2.7 and Pygame, so those are likely the requirements to run ```bp.py```.

### Features ###

* Faithfully reproduced plane graphics for both the green and red "barons".
* Flight physics, with momentum and plane stalling.
* 2-Player, simultaneous controls. 
* Bullet physics, enabling each plane to shoot projectiles

### Not Implemented ###
* Clouds, hot-air balloon, tower, crash animations
* Scoring/Victory conditions
* Plane hit-boxes (bullets do no damage)
* Sound effects